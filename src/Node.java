public class Node {

    Node following = null;

    int data;

    public Node(int data) {
        this.data = data;
    }

    public void appendToEnd(int data) {
        Node end = new Node(data);
        Node n = this;

        while (n.following != null) {
            n = n.following;
        }
        n.following = end;
    }

    // TODO:: Implement to return the length of the SinglyLinkedList
    // For example:: --> 5 --> 6 --> 7 --> 3 --> .
    public void printNodes() {
        Node n = this;
        System.out.print(n.data);
        while (n.following!=null){
            n=n.following;
            System.out.print(" --> ");
            System.out.print(n.data);
        }
    }

    // TODO:: Implement to return the length of the SinglyLinkedList
    int length(Node h) {
        Node n=h;
        int i=1;
        while (n.following!=null){
            n=n.following;
            i++;
        }
        return i;
    }

    // TODO:: Implement to return the sum of the Nodes
    int sumOfNodes() {
        Node n=this;
        int sum=n.data;
        while (n.following!=null){
            n=n.following;
            sum+=n.data;
        }
        return sum;
    }

    Node deleteNode(Node head, int data) {
        Node n = head;
        Node tmp=null;
        if (n.data == data) {
            return head.following;
        }
        while (n!=null && n.data!=data){
            tmp=n;
            n=n.following;
        }
        if(tmp.following != null){
            tmp.following=n.following;
        }

        return head;
    }
}