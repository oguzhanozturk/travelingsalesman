public class Main {
    public static void main(String[] args) {
        // Example
        Node node = new Node(4);
        node.appendToEnd(5);
        node.appendToEnd(6);
        node.appendToEnd(7);

        System.out.println(node.sumOfNodes());

        node.deleteNode(node,4);
        node.deleteNode(node,5);
        node.deleteNode(node,6);
        node.deleteNode(node,7);

        // End of Example
    }
}
